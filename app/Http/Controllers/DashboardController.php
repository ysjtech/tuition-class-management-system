<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Classes;
use App\Attendance;
use App\StudentClass;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        date_default_timezone_set('Asia/Colombo');

        // admin
        // ==================================
        $adminDashboard = array ();
        $totalStudents = User::where('user_type', 'student')->whereNull('is_deleted')->count();
        $totalTutors = User::where('user_type', 'tutor')->whereNull('is_deleted')->count();
        $totalClasses = Classes::whereNull('is_deleted')->count();
        $todayClasses = Classes::where('day', date("l"))->whereNull('is_deleted')->get();
        
        foreach ($todayClasses as $tc) {
            $tc['tutor'] = User::find($tc->tutor_id);
            $classAttendanceCount = Attendance::where ('class_id', $tc->id)->where ('class_date', date("Y-m-d"))->count();
            $tc['studentcount'] = $classAttendanceCount;
        }
        
        $adminDashboard['totalstudents'] = $totalStudents;
        $adminDashboard['totaltutors'] = $totalTutors;
        $adminDashboard['totalclasses'] = $totalClasses;
        $adminDashboard['todayclasses'] = $todayClasses;
        
        // tutor
        // ==================================
        $tutorDashboard = array ();
        
        $myClassesInfo = Classes::where('tutor_id', auth()->user()->id)->whereNull('is_deleted')->get();
        $myStudents = 0;
        foreach ($myClassesInfo as $c) {
            $myStudents += StudentClass::where('class_id', $c->id)->count();
        }
        
        $myClasses = Classes::where('tutor_id', auth()->user()->id)->whereNull('is_deleted')->count();
        
        $todayTutorClasses = Classes::where('day', date("l"))->where('tutor_id', auth()->user()->id)->whereNull('is_deleted')->get();
        
        foreach ($todayTutorClasses as $c) {
            
            $classAttendanceCount = Attendance::where ('class_id', $c->id)->where ('class_date', date("Y-m-d"))->count();
            $c['studentcount'] = $classAttendanceCount;
            
        }
        
        $tutorDashboard['mystudents'] = $myStudents;
        $tutorDashboard['myclasses'] = $myClasses;
        $tutorDashboard['todayclassattendance'] = $todayTutorClasses;
        
        // student
        // ==================================
        $studentDashboard = array ();
        
        $studentClasses = StudentClass::where('student_id', auth()->user()->id)->count();
        
        $todayAllClasses = Classes::where('day', date("l"))->whereNull('is_deleted')->get();
        $studentTodayClassesInfo = StudentClass::where('student_id', auth()->user()->id)->get();
        $studentTodayClasses = array ();

        foreach ($todayAllClasses as $c) {            
            foreach ($studentTodayClassesInfo as $stc) {
                if ($c->id == $stc->class_id) {
                    $c['tutor'] = User::find($c->tutor_id);
                    $studentTodayClasses[] = $c;
                    continue;
                }
            }
        }
        
        $studentDashboard['myclasses'] = $studentClasses;
        $studentDashboard['todayclasses'] = $studentTodayClasses;
        
        return view('dashboard.dashboard', ['admindashboard' => $adminDashboard, 'tutordashboard' => $tutorDashboard, 'studentdashboard' => $studentDashboard]);
        
    }
}
