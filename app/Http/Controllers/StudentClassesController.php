<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Classes;
use App\StudentClass;
use DataTables;

class StudentClassesController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $classes = Classes::all();
        $studentclasses = StudentClass::where ('student_id', $id)->get();
        
        return view('studentclasses.classes_edit', ['student_id' => $id, 'classes' => $classes, 'studentclasses' => $studentclasses]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        try {
            
            StudentClass::where ('student_id', $id)->delete();
            
            if ($request->submit) {
                
                $classes = array ();
                
                if (isset($request->class)) {
                    
                    foreach ($request->class as $c) {
                        
                        $class = array (
                            'student_id' => $id,
                            'class_id' => $c
                        );
                        
                        $classes[] = $class;
                        
                    }
                    
                }
                
                $classesRes = StudentClass::insert($classes);
                
                if ($classesRes) {
                    return redirect()->route('student.classes.edit', ['id' => $id])->with('success', 'Classes updated successfully!');
                } else {
                    return redirect()->route('student.classes.edit', ['id' => $id])->with('success', 'Class updated error!');
                }
                
            }
            
        } catch (\Exception $ex) {
            // do task when error
            echo $ex->getMessage();
        }
            
    }
        
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
