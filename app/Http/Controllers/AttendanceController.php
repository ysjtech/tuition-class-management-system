<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attendance;
use App\User;
use App\Classes;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'class'  => 'required',
            'student' => 'required'
        ]);

        try {

            if ($request->submit) {

                $attendance = new Attendance();
                $attendance->class_id = $request->class;
                $attendance->student_id = $request->student;
                $attendance->class_date = date("Y-m-d");
                $attendance->created_at = new \DateTime();
                $attendance->updated_at = new \DateTime();
                $attendance->save();

                return redirect()->route('attendance')->with('success', 'Attendance marked successfully!');
                
            }

        } catch (\Exception $ex) {
            // do task when error
            echo $ex->getMessage();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
        $classes = Classes::where('day', date("l"))->whereNull('is_deleted')->get(); // today classes
        $students = User::where ('user_type', 'student')->get(); // student for today classes
        
        return view('attendance.attendance', ['classes' => $classes, 'students' => $students]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
