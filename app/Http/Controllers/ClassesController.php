<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Classes;
use App\StudentClass;
use App\ClassMaterial;
use DataTables;

class ClassesController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $user = auth()->user();
        
        if ($request->ajax()) {
        
            if ($user->user_type == 'tutor') {
                $classes = Classes::where ('tutor_id', $user->id)->whereNull('is_deleted')->get();
            } elseif ($user->user_type == 'student') {
                $studentclasses = StudentClass::where ('student_id', $user->id)->get();
                $studentClassesArray = array();
                foreach ($studentclasses as $sc) {
                    $studentClassesArray[] = $sc->class_id;
                }
                $classes = Classes::find ($studentClassesArray)->whereNull('is_deleted');
            } else {
                $classes = Classes::all()->whereNull('is_deleted');
            }
            
            return Datatables::of($classes)
//                    ->addIndexColumn()
                    ->addColumn('tutor', function($row) {
                        $tutorInfo = User::find($row['tutor_id']);
                        return $tutorInfo['first_name'].' '.$tutorInfo['last_name'];
                    })
                    ->addColumn('action', function($row) {
                        $editUrl = route('classes.edit', ['id' => $row->id]);//
                        $editBtn = '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm mr-2"><i class="fas fa-edit"></i></a>';
                        
                        $deleteUrl = route('classes.delete', ['id' => $row->id]);
                        $deleteBtn = '<form action="'.$deleteUrl.'" method="post">';
                        $deleteBtn .= csrf_field();
                        $deleteBtn .= '<button class="btn btn-danger btn-sm" name="submit" value="submit" type="submit"><i class="fas fa-trash-alt"></i></button></form>';
                        
                        $viewUrl = route('classes.view', ['id' => $row->id]);
                        $viewBtn = '<a href="'.$viewUrl.'" class="edit btn btn-success btn-sm"><i class="fas fa-eye"></i></a>';
                        
                        if (auth()->user()->user_type == 'student') {
                            return '<div style="display: flex;">'.$viewBtn.'</div>';
                        } elseif ((auth()->user()->user_type == 'tutor')) {
                            return '<div style="display: flex;">'.$editBtn.'</div>';
                        } else {
                            return '<div style="display: flex;">'.$editBtn.' '.$deleteBtn.'</div>';
                        }
                        
                    })
                    ->rawColumns(['action'])
                    ->make(true);
                    
        }

        return view('classes.classes');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $tutors = User::where ('user_type', 'tutor')->get();

        return view('classes.class_add', ['tutors' => $tutors]);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'name'  => 'required',
            'subject' => 'required',
            'grade'  => 'required',
            'tutor'  => 'required',
            'day'  => 'required',
            'from'  => 'required',
            'to'  => 'required'
        ]);

        if ($request->submit) {

            $class = new Classes();
            $class->name = $request->name;
            $class->subject = $request->subject;
            $class->grade = $request->grade;
            $class->tutor_id = $request->tutor;
            $class->day = $request->day;
            $class->from = $request->from;
            $class->to = $request->to;
            if ($request->description) {
                $class->description = $request->description;
            }
            $class->created_at = new \DateTime();
            $class->updated_at = new \DateTime();

            try {
                $class->save();
                return redirect()->route('classes.index')->with('success', 'Class added successfully!');
            } catch (\Exception $ex) {
                // do task when error
                echo $ex->getMessage();
            }
            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $class = Classes::find($id);
        $tutors = User::where ('user_type', 'tutor')->get();
        
        $materials = ClassMaterial::where('class_id', $id)->get();

        return view('classes.class_view', ['class' => $class, 'tutors' => $tutors, 'materials' => $materials]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $class = Classes::find($id);
        $tutors = User::where ('user_type', 'tutor')->get();
        
        $materials = ClassMaterial::where('class_id', $id)->get();

        return view('classes.class_edit', ['class' => $class, 'tutors' => $tutors, 'materials' => $materials]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'name'  => 'required',
            'subject' => 'required',
            'grade'  => 'required',
            'tutor'  => 'required',
            'day'  => 'required',
            'from'  => 'required',
            'to'  => 'required'
        ]);

        if ($request->submit) {

            $class = Classes::find($id);
            $class->name = $request->name;
            $class->subject = $request->subject;
            $class->grade = $request->grade;
            $class->tutor_id = $request->tutor;
            $class->day = $request->day;
            $class->from = $request->from;
            $class->to = $request->to;
            if ($request->description) {
                $class->description = $request->description;
            }
            
            if (isset($request->material)) {
                
                $classMaterial = new ClassMaterial();
                $classFileName = str_replace(' ', '-', strtolower($request->material->getClientOriginalName()));
                $classMaterialName = time().'-'.$classFileName; //.'-'.time().'.'.$request->material->extension();
                $request->material->move(public_path('class-materials'), $classMaterialName);

                $fileType = '';
                $imgFormats = array('gif', 'png', 'jpg');
                $docFormats = array('pdf', 'doc', 'docx', 'xls');
                if (in_array($request->material->getClientOriginalExtension(), $imgFormats)) {
                    $fileType = 'img';
                } elseif (in_array($request->material->getClientOriginalExtension(), $docFormats)) {
                    $fileType = 'doc';
                } else {
                    $fileType = 'other';
                }

                $classMaterial->class_id = $id;
                $classMaterial->type = $fileType;
                $classMaterial->url = $classMaterialName;
                $classMaterial->created_at = new \DateTime();
                $classMaterial->updated_at = new \DateTime();
                $classMaterial->save();
            
            }

            try {
                $class->save();
                return redirect()->route('classes.index')->with('success', 'Class updated successfully!');
            } catch (\Exception $ex) {
                // do task when error
                echo $ex->getMessage();
            }
            
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        
        try {

            if ($request->submit) {

                $user = Classes::find($id);
                $user->is_deleted = true;
                $user->save();

                return back()->with('success', 'Class deleted successfully!');

            }

        } catch (\Exception $ex) {
            // do task when error
            echo $ex->getMessage();
        }
        
    }
    
    
}
