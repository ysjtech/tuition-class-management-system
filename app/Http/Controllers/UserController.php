<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DataTables;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    
    use RegistersUsers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if ($request->ajax()) {

            // $users = User::all();
            $users = User::whereNotIn('user_type', ['student', 'tutor']);
            
            return Datatables::of($users)
                    // ->addIndexColumn()
                    ->addColumn('name', function($row){
                        $firstname = $row->first_name;
                        $lastname = $row->last_name;
                        return $firstname." ".$lastname;
                    })
                    ->addColumn('user_type', function($row){
                        $userType = ucwords($row->user_type);
                        return $userType;
                    })
                    ->addColumn('action', function($row){
                        $url = route('users.edit', ['id' => $row->id]);
                        $editBtn = '<a href="'.$url.'" class="edit btn btn-primary btn-sm mr-2"><i class="fas fa-edit"></i></a>';
                        // $deleteBtn = '<a href="javascript:void(0)" class="edit btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
                        $deleteUrl = route('users.delete', ['id' => $row->id]);
//                        $deleteBtn = '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
                        $deleteBtn = '<form action="'.$deleteUrl.'" method="post">';
                        $deleteBtn .= csrf_field();
                        $deleteBtn .= '<button class="btn btn-danger btn-sm" name="submit" value="submit" type="submit"><i class="fas fa-trash-alt"></i></button></form>';
                        
                        return '<div style="display: flex;">'.$editBtn." ".$deleteBtn.'</div>';
                    })
                    ->rawColumns(['name'])
                    ->rawColumns(['user_type'])
                    ->rawColumns(['action'])
                    ->make(true);
                    
        }

        return view('user.users', ['type' => 'user']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function students(Request $request)
    {
        
        if ($request->ajax()) {

            $users = User::where('user_type', 'student')->whereNull('is_deleted');
            
            return Datatables::of($users)
                    // ->addIndexColumn()
                    ->addColumn('name', function($row){
                        $firstname = $row->first_name;
                        $lastname = $row->last_name;
                        return $firstname." ".$lastname;
                    })
                    ->addColumn('user_type', function($row){
                        $userType = ucwords($row->user_type);
                        return $userType;
                    })
                    ->addColumn('action', function($row){
                        $url = route('users.edit', ['id' => $row->id]);
                        $editBtn = '<a href="'.$url.'" class="edit btn btn-primary btn-sm mr-2"><i class="fas fa-edit"></i></a>';
                        // $deleteBtn = '<a href="javascript:void(0)" class="edit btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
                        $scurl = route('student.classes.edit', ['id' => $row->id]);
                        $scurlBtn = '<a href="'.$scurl.'" class="edit btn btn-success btn-sm mr-2"><i class="fas fa-book-open"></i></a>';
                        
                        $deleteUrl = route('users.delete', ['id' => $row->id]);
//                        $deleteBtn = '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
                        $deleteBtn = '<form action="'.$deleteUrl.'" method="post">';
                        $deleteBtn .= csrf_field();
                        $deleteBtn .= '<button class="btn btn-danger btn-sm" name="submit" value="submit" type="submit"><i class="fas fa-trash-alt"></i></button></form>';
                        
                        return '<div style="display: flex;">'.$editBtn." ".$scurlBtn." ".$deleteBtn.'</div>';
                    })
                    ->rawColumns(['name'])
                    ->rawColumns(['user_type'])
                    ->rawColumns(['action'])
                    ->make(true);
                    
        }

        return view('user.users', ['type' => 'student']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tutors(Request $request)
    {
        
        if ($request->ajax()) {

            $users = User::where('user_type', 'tutor')->whereNull('is_deleted');
            
            return Datatables::of($users)
                    // ->addIndexColumn()
                    ->addColumn('name', function($row){
                        $firstname = $row->first_name;
                        $lastname = $row->last_name;
                        return $firstname." ".$lastname;
                    })
                    ->addColumn('user_type', function($row){
                        $userType = ucwords($row->user_type);
                        return $userType;
                    })
                    ->addColumn('action', function($row){
                        $url = route('users.edit', ['id' => $row->id]);
                        $editBtn = '<a href="'.$url.'" class="edit btn btn-primary btn-sm mr-2"><i class="fas fa-edit"></i></a>';
                        
                        $deleteUrl = route('users.delete', ['id' => $row->id]);
//                        $deleteBtn = '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
                        $deleteBtn = '<form action="'.$deleteUrl.'" method="post">';
                        $deleteBtn .= csrf_field();
                        $deleteBtn .= '<button class="btn btn-danger btn-sm" name="submit" value="submit" type="submit"><i class="fas fa-trash-alt"></i></button></form>';
                        
                        return '<div style="display: flex;">'.$editBtn." ".$deleteBtn.'</div>';
                    })
                    ->rawColumns(['name'])
                    ->rawColumns(['user_type'])
                    ->rawColumns(['action'])
                    ->make(true);
                    
        }

        return view('user.users', ['type' => 'tutor']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('user.user_add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'firstname'  => 'required',
            'lastname' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'usertype'  => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        try {

            $userType = $request->usertype;

            if ($request->submit) {

                $user = new User();
                $user->first_name = $request->firstname;
                $user->last_name = $request->lastname;
                $user->contact_no = $request->contactno;
                $user->school_name = $request->school;
                $user->grade = $request->grade;
                $user->email = $request->email;
                $user->user_type = $request->usertype;
                $user->password = Hash::make($request->password);

                $user->save();

                if ($userType == 'student') {
                    return redirect()->route('users.students')->with('success', 'Student added successfully!');
                } elseif ($userType == 'tutor') {
                    return redirect()->route('users.tutors')->with('success', 'Tutor added successfully!');
                } else {
                    return redirect()->route('users.index')->with('success', 'User added successfully!');
                }
                
            }

        } catch (\Exception $ex) {
            // do task when error
            echo $ex->getMessage();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $user = User::find($id);
        
        return view('user.user_edit', ['user' => $user]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'firstname'  => 'required',
            'lastname' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
            'usertype'  => 'required',
            'password' => 'confirmed',
        ]);

        try {

            $userType = $request->usertype;

            if ($request->submit) {

                $user = User::find($id);
                $user->first_name = $request->firstname;
                $user->last_name = $request->lastname;
                $user->contact_no = $request->contactno;
                $user->school_name = $request->school;
                $user->grade = $request->grade;
                $user->email = $request->email;
                $user->user_type = $request->usertype;
                $user->password = Hash::make($request->password);
                $user->updated_at = new \DateTime();
                $user->save();

                if ($userType == 'student') {
                    return redirect()->route('users.students')->with('success', 'Student updated successfully!');
                } elseif ($userType == 'tutor') {
                    return redirect()->route('users.tutors')->with('success', 'Tutor updated successfully!');
                } else {
                    return redirect()->route('users.index')->with('success', 'User updated successfully!');
                }

            }

        } catch (\Exception $ex) {
            // do task when error
            echo $ex->getMessage();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        
        try {

            if ($request->submit) {

                $user = User::find($id);
                $user->is_deleted = true;
                $user->save();

                return back()->with('success', 'User deleted successfully!');

            }

        } catch (\Exception $ex) {
            // do task when error
            echo $ex->getMessage();
        }
        
    }
    
}
