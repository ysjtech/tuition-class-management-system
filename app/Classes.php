<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'classes';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
}
