<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassMaterial extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'class_materials';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
}
