@extends('layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Class</li>
    </ol>
    
    <div class="row">
        
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">

                <div class="card-body">
                    
                    <h3>{{$class->name}}</h3>
                    
                    <h5 class="mb-2">{{$class->subject}} - Grade {{$class->grade}}</h5>
                    
                    <?php
                        foreach ($tutors as $t) {
                    ?>
                        @if ($t['id'] == $class->tutor_id)
                            By <span class="badge badge-primary mb-2">{{$t['first_name'].' '.$t['last_name']}}</span>
                        @endif
                    <?php
                        }
                    ?>
                        
                    <h6 class="mt-2 mb-4">
                        <span class="badge badge-success">{{ $class->day }}</span> 
                        From <span class="badge badge-info">{{$class->from}}</span>
                        To <span class="badge badge-info">{{$class->to}} </span>
                    </h6>
                    
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-dark">Materials</li>
                        @foreach ($materials as $material)
                        <li class="list-group-item">
                            {{ucfirst(preg_replace('/\\.[^.\\s]{3,4}$/', '', $material->url))}}
                            <a href="{{asset('class-materials/'.$material->url)}}" target="_blank"><i class="ml-2 fas fa-download"></i></a>
                        </li>
                        @endforeach
                    </ul>
                    
                    <p class="border" style="padding: 15px;">
                        {{$class->description}}
                    </p>

                </div>

            </div>
            
        </div>
        
    </div>
    
</div>
@endsection