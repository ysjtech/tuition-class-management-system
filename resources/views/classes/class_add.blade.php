@extends('layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Class</li>
    </ol>
    
    <div class="row">
        
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">

                <div class="card-body">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('/classes') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Enter name" id="name">
                        </div>
                        
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" name="subject" class="form-control" placeholder="Enter subject" id="subject">
                        </div>
                        
                        <div class="form-group">
                            <label for="grade">Grade</label>
                            <input type="text" name="grade" class="form-control" placeholder="Enter grade" id="grade">
                        </div>
                        
                        <div class="form-group">
                            <label for="tutor">Tutor</label>
                            <select name="tutor" class="form-control" id="tutor">
                                <option selected="selected" disabled="disabled">Select a Tutor</option>

                                <?php
                                    foreach ($tutors as $t) {
                                ?>
                                    <option value="<?php echo $t['id']; ?>"><?php echo $t['first_name'].' '.$t['last_name']; ?></option>
                                <?php
                                    }
                                ?>

                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="day">Day</label>
                            <select name="day" class="form-control" id="day">
                                <option selected="selected" disabled="disabled">Select a Day</option>
                                <option value="Sunday">Sunday</option>
                                <option value="Monday">Monday</option>
                                <option value="Tuesday">Tuesday</option>
                                <option value="Wednesday">Wednesday</option>
                                <option value="Thursday">Thursday</option>
                                <option value="Friday">Friday</option>
                                <option value="Saturday">Saturday</option>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="from">From</label>
                            <input type="time" id="from" class="form-control" name="from">
                        </div>
                        
                        <div class="form-group">
                            <label for="to">To</label>
                            <input type="time" id="to" class="form-control" name="to">
                        </div>
                        
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" class="form-control" rows="5" id="description"></textarea>
                        </div>
                        
                        <button name="submit" value="submit" type="submit" class="btn btn-primary">Add</button>
                        
                    </form>

                </div>

            </div>
            
        </div>
        
    </div>
    
</div>
@endsection