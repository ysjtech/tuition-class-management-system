@extends('layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Class</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">

                <div class="card-body">
                    
                    <div class="table-responsive">
                        <table class="table table-bordered data-table" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <!--<th>ID</th>-->
                                    <th>Name</th>
                                    <th>Tutor</th>
                                    <th>Subject</th>
                                    <th>Grade</th>
                                    
                                    <th></th>
<!--                                    <th>Age</th>
                                    <th>Start date</th>
                                    <th>Salary</th>-->
                                </tr>
                            </thead>
<!--                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Age</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                            </tfoot>-->
                            <tbody>
                                
<!--                                <tr>
                                    <td>Tiger Nixon</td>
                                    <td>System Architect</td>
                                    <td>Edinburgh</td>
                                    <td>61</td>
                                    <td>2011/04/25</td>
                                    <td>$320,800</td>
                                </tr>-->
                                
                                
                            </tbody>
                        </table>
                    </div>
                    
<!--                    <form action="/action_page.php">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" placeholder="Enter name" id="name">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" rows="5" id="description"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>-->

                </div>

            </div>
            
        </div>
    </div>
    
</div>

@endsection

@section('javascript')

<script type="text/javascript">
    
    $( document ).ready(function() {
        
        $(function () {
    
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('classes.index') }}",
                columns: [
//                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'tutor', name: 'tutor'},
                    {data: 'subject', name: 'subject'},
                    {data: 'grade', name: 'grade'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
        
    });
  
</script>

@endsection