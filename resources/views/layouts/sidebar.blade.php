<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <!--<div class="sb-sidenav-menu-heading">Core</div>-->
                <a class="nav-link" href="{{url('dashboard')}}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-tachometer-alt"></i>
                    </div>
                    Dashboard
                </a>
                
                @if (auth()->user()->user_type == 'admin')
                <a class="nav-link" href="{{url('attendance')}}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-users"></i>
                    </div>
                    Attendance
                </a>
                @endif
                
                @if (auth()->user()->user_type == 'admin')
                <a class="nav-link" href="{{url('calendar')}}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-calendar-alt"></i>
                    </div>
                    Calendar
                </a>
                @endif
                
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseClasses" aria-expanded="false" aria-controls="collapseClasses">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                        Classes
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseClasses" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{url('/classes')}}">View All</a>
                        @if (auth()->user()->user_type == 'admin')
                        <a class="nav-link" href="{{url('classes/create')}}">Add New</a>
                        @endif
                    </nav>
                </div>
                
                <!-- <div class="sb-sidenav-menu-heading pt-2 pb-2">Process</div> -->
                @if (auth()->user()->user_type == 'admin')
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsers" aria-expanded="false" aria-controls="collapseUsers">
                    <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                        Users
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseUsers" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{url('/users/students')}}">Students</a>
                        <a class="nav-link" href="{{url('/users/tutors')}}">Tutors</a>
                        <a class="nav-link" href="{{url('/users')}}">System Users</a>
                        <a class="nav-link" href="{{url('users/create')}}">Add New</a>
                    </nav>
                </div>
                @endif
                <!-- <a class="nav-link" href="{{url('dashboard')}}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-cog"></i>
                    </div>
                    Settings
                </a> -->
            </div>
        </div>
<!--        <div class="sb-sidenav-footer">
            <div class="small">Logged in as:</div>
            Start Bootstrap
        </div>-->
    </nav>
</div>