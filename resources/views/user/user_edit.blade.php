@extends('layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Users</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">

                <div class="card-body">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('/users/'.$user->id.'/update') }}" method="POST">
                        @csrf

                        <div class="form-group">
                            <label for="firstname">Firstname<span class="text-danger">*</span></label>
                            <input type="text" name="firstname" value="{{$user->first_name}}" class="form-control" placeholder="Enter first name" id="firstname">
                        </div>

                        <div class="form-group">
                            <label for="lastname">Lastname<span class="text-danger">*</span></label>
                            <input type="text" name="lastname" value="{{$user->last_name}}" class="form-control" placeholder="Enter last name" id="lastname">
                        </div>

                        <div class="form-group">
                            <label for="contactno">Contact No.</label>
                            <input type="text" name="contactno" value="{{$user->contact_no}}" class="form-control" placeholder="Enter contact no." id="contactno">
                        </div>

                        <div class="form-group">
                            <label for="school">School</label>
                            <input type="text" name="school" value="{{$user->school_name}}" class="form-control" placeholder="Enter school" id="school">
                        </div>

                        <div class="form-group">
                            <label for="grade">Grade</label>
                            <input type="text" name="grade" value="{{$user->grade}}" class="form-control" placeholder="Enter grade" id="grade">
                        </div>

                        <div class="form-group">
                            <label for="email">Email<span class="text-danger">*</span></label>
                            <input type="email" name="email" value="{{$user->email}}" class="form-control" placeholder="Enter email" id="email">
                        </div>

                        <div class="form-group">
                            <label for="usertype">User Type<span class="text-danger">*</span></label>
                                <select name="usertype" class="form-control" id="usertype">
                                    <option disabled="disabled">Select user type</option>
                                    <option value="student" {{ $user->user_type == 'student' ? "selected" : '' }}>Student</option>
                                    <option value="tutor" {{ $user->user_type == 'tutor' ? "selected" : '' }}>Tutor</option>
                                    <option value="admin" {{ $user->user_type == 'admin' ? "selected" : '' }}>Administrator</option>
                                </select>
                        </div>

                        <div class="form-group">
                            <label for="password">Password<span class="text-danger">*</span></label>
                            <input type="password" name="password" class="form-control" placeholder="Enter password" id="password">
                        </div>

                        <div class="form-group">
                            <label for="confpassword">Confirm password<span class="text-danger">*</span></label>
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Enter confirm password" id="confpassword">
                        </div>

                        <button name="submit" value="submit" type="submit" class="btn btn-primary">Update</button>
                    </form>

                </div>

            </div>
            
        </div>
    </div>
    
</div>

@endsection