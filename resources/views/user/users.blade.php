@extends('layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Users</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">

                <div class="card-body">
                    
                    <div class="table-responsive">
                        <table class="table table-bordered data-table" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Type</th>
                                    <th></th>
<!--                                    <th>Age</th>
                                    <th>Start date</th>
                                    <th>Salary</th>-->
                                </tr>
                            </thead>
<!--                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Age</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                            </tfoot>-->
                            <tbody>
                                
<!--                                <tr>
                                    <td>Tiger Nixon</td>
                                    <td>System Architect</td>
                                    <td>Edinburgh</td>
                                    <td>61</td>
                                    <td>2011/04/25</td>
                                    <td>$320,800</td>
                                </tr>-->
                                
                                
                            </tbody>
                        </table>
                    </div>
                    
<!--                    <form action="/action_page.php">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" placeholder="Enter name" id="name">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" rows="5" id="description"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>-->

                </div>

            </div>
            
        </div>
    </div>
    
</div>

@endsection

@section('javascript')

<script type="text/javascript">

    var userType = '{{$type}}';
    var url = "{{ route('users.index') }}";
    
    if (userType == 'student') {
        url = "{{ route('users.students') }}";
    } else if (userType == 'tutor') {
        url = "{{ route('users.tutors') }}";
    }
    
    $( document ).ready(function() {
        
        $(function () {
    
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'user_type', name: 'user_type'},
                    // {data: 'description', name: 'description'},
                    // {data: 'email', name: 'email'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
        
    });
  
</script>

@endsection