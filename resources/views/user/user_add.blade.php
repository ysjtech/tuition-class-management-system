@extends('layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Users</li>
    </ol>
    
    <div class="row">
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">

                <div class="card-body">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('/users') }}" method="POST">
                        @csrf

                        <div class="form-group">
                            <label for="usertype">User Type<span class="text-danger">*</span></label>
                                <select name="usertype" class="form-control" id="usertype">
                                    <option selected="selected" disabled="disabled">Select user type</option>
                                    <option value="student">Student</option>
                                    <option value="tutor">Tutor</option>
                                    <option value="admin">Administrator</option>
                                </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="firstname">Firstname<span class="text-danger">*</span></label>
                            <input type="text" name="firstname" class="form-control" placeholder="Enter first name" id="firstname">
                        </div>

                        <div class="form-group">
                            <label for="lastname">Lastname<span class="text-danger">*</span></label>
                            <input type="text" name="lastname" class="form-control" placeholder="Enter last name" id="lastname">
                        </div>

                        <div class="form-group">
                            <label for="contactno">Contact No.</label>
                            <input type="text" name="contactno" class="form-control" placeholder="Enter contact no." id="contactno">
                        </div>

                        <div class="form-group" id="school-sec">
                            <label for="school">School</label>
                            <input type="text" name="school" class="form-control" placeholder="Enter school" id="school">
                        </div>

                        <div class="form-group" id="grade-sec">
                            <label for="grade">Grade</label>
                            <input type="text" name="grade" class="form-control" placeholder="Enter grade" id="grade">
                        </div>

                        <div class="form-group">
                            <label for="email">Email<span class="text-danger">*</span></label>
                            <input type="email" name="email" class="form-control" placeholder="Enter email" id="email">
                        </div>

                        <div class="form-group">
                            <label for="password">Password<span class="text-danger">*</span></label>
                            <input type="password" name="password" class="form-control" placeholder="Enter password" id="password">
                        </div>

                        <div class="form-group">
                            <label for="confpassword">Confirm password<span class="text-danger">*</span></label>
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Enter confirm password" id="confpassword">
                        </div>

                        <button name="submit" value="submit" type="submit" class="btn btn-primary">Add</button>
                    </form>

                </div>

            </div>
            
        </div>
    </div>
    
</div>

@endsection

@section('javascript')

<script>
    
    $('#usertype').on('change', function() {
        
        switch($(this).val()) {
            case 'student':
                $('#school-sec').show();
                $('#grade-sec').show();
                break;
            case 'tutor':
                $('#school-sec').hide();
                $('#grade-sec').hide();
                break;
            default:
                $('#school-sec').hide();
                $('#grade-sec').hide();
        }

    });
    
</script>

@endsection