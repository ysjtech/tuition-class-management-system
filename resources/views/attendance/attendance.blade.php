@extends('layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Student Attendance</li>
    </ol>
    
    <div class="row">
        
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">

                <div class="card-body">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-inline" action="{{ url('/attendance') }}" method="POST" style="justify-content: space-between;">
                        @csrf
                        
                        <div class="form-group" style="width: 39%;">
                            <select name="class" class="form-control" id="class" style="width: 100%;">
                                <option selected="selected" disabled="disabled">Select a Class</option>

                                <?php
                                    foreach ($classes as $c) {
                                ?>
                                    <option value="<?php echo $c['id']; ?>"><?php echo $c['name']; ?></option>
                                <?php
                                    }
                                ?>

                            </select>
                        </div>
                        
                        <div class="form-group" style="width: 39%;">
                            <select name="student" class="form-control" id="student" style="width: 100%;">
                                <option selected="selected" disabled="disabled">Select a Student</option>

                                <?php
                                    foreach ($students as $s) {
                                ?>
                                    <option value="<?php echo $s['id']; ?>"><?php echo $s['first_name'].' '.$s['last_name']; ?></option>
                                <?php
                                    }
                                ?>

                            </select>
                        </div>
                        
                        <button name="submit" value="submit" type="submit" class="btn btn-primary"  style="width: 20%;">Mark</button>
                        
                    </form>

                </div>

            </div>
            
        </div>
        
        <div class="col-xl-12 col-md-12">
            

            
        </div>
        
    </div>
    
</div>
@endsection