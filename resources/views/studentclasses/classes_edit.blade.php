@extends('layouts.app')

@section('content')

<div class="container-fluid">
    
    <ol class="breadcrumb mb-4 mt-4">
        <li class="breadcrumb-item active">Student Classes</li>
    </ol>
    
    <div class="row">
        
        <div class="col-xl-12 col-md-12">
            
            <div class="card mb-4">

                <div class="card-body">
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('/student/classes/'.$student_id.'/update') }}" method="POST">
                        @csrf
                        
                        <div class="row justify-content-end pr-3">
                            <button name="submit" value="submit" type="submit" class="btn btn-primary mb-3">Update Classes</button>
                        </div>
                        
                        <div class="row">
                            
                            <?php
                                foreach ($classes as $c) {
                                    
                                    $availableClass = false;
                                    
                                    foreach ($studentclasses as $sc) {
                                        
                                        if ($c->id == $sc->class_id) {
                                            $availableClass = true;
                                        }
                                        
                                    }
                                    
                            ?>
                            
                            <div class="col-3 mb-2">
                                <div class="card text-center" style="{{ ($availableClass == true) ? 'background-color:#bbb898;' : '' }}}">
                                    <div class="card-header">
                                        <h4>{{$c->name}}</h4>
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">{{$c->subject}}</h5>
                                        <h6 class="card-title">{{$c->grade}}</h6>
                                        <p class="card-text">{{$c->description}}</p>
                                        <p class="card-text">{{$c->tutor_id}}</p>
                                    </div>
                                    <div class="card-footer" style="background-color: #171515; color: beige !important;">
                                        <div class="form-group form-check mb-0">
                                            <input type="checkbox" class="form-check-input" value="{{$c->id}}" name="class[]" id="selectClass" {{ ($availableClass == true) ? 'checked' : '' }}>
                                            <label class="form-check-label" for="selectClass">Select Class</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <?php
                                }
                            ?>
                            
                        </div>
                        
                    </form>

                </div>

            </div>
            
        </div>
        
    </div>
    
</div>
@endsection