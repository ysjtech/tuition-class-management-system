@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <!--<h1 class="mt-4">Dashboard</h1>-->
    <ol class="breadcrumb mt-4 mb-4">
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    
    @if (auth()->user()->user_type == 'admin')
    
        <div class="row">

            <div class="col-xl-3 col-md-6">
                <div class="card bg-primary text-white mb-4">
                    <div class="card-body">Total Students</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        {{$admindashboard['totalstudents']}}
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card bg-warning text-white mb-4">
                    <div class="card-body">Total Tutors</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        {{$admindashboard['totaltutors']}}
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card bg-success text-white mb-4">
                    <div class="card-body">Total Classes</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        {{$admindashboard['totalclasses']}}
                    </div>
                </div>
            </div>

        </div>
    
    @endif
    
<!--    <div class="row">
        <div class="col-xl-6">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-chart-area mr-1"></i>
                    Area Chart Example
                </div>
                <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-chart-bar mr-1"></i>
                    Bar Chart Example
                </div>
                <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
            </div>
        </div>
    </div>-->

    @if (auth()->user()->user_type == 'admin')
    
        <div class="row">
            <div class="col-12 col-md-12 mb-2">
                <h5>Today Class Schedule</h5>
            </div>

            @foreach ($admindashboard['todayclasses'] as $class)
                <div class="col-4 col-md-4">

                    <div class="card mb-2">
                        <h5 class="card-header">{{$class->name}}<span class="ml-2 badge badge-primary">Attendance - {{$class->studentcount}}</span></h5>
                        <div class="card-body">
                            <h6 class="card-title">{{$class->subject}} - Grade {{$class->grade}}</h6>
                            <p class="card-text">By <b>{{$class->tutor['first_name']}} {{$class->tutor['last_name']}}</b></p>
                            <p class="card-text">
                                <span class="badge badge-success">{{$class->day}}</span> From <span class="badge badge-info">{{$class->from}}</span> To <span class="badge badge-info">{{$class->to}}</span>
                            </p>
                        </div>
                    </div>

                </div>
            @endforeach

        </div>
    
    @endif
    
    @if (auth()->user()->user_type == 'tutor')
    
        <div class="row">

            <div class="col-xl-3 col-md-6">
                <div class="card bg-primary text-white mb-4">
                    <div class="card-body">My Students</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        {{$tutordashboard['mystudents']}}
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card bg-warning text-white mb-4">
                    <div class="card-body">My Classes</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        {{$tutordashboard['myclasses']}}
                    </div>
                </div>
            </div>

        </div>
    
        <div class="row">
            <div class="col-12 col-md-12 mb-2">
                <h5>Today Classes</h5>
            </div>

            @foreach ($tutordashboard['todayclassattendance'] as $class)
                <div class="col-4 col-md-4">

                    <div class="card mb-2">
                        <h5 class="card-header">{{$class->name}} <span class="badge badge-primary">Attendance - {{$class->studentcount}}</span></h5>
                        <div class="card-body">
                            <h6 class="card-title">{{$class->subject}} - Grade {{$class->grade}}</h6>
                            <!--<p class="card-text">By <b>{{$class->tutor['first_name']}} {{$class->tutor['last_name']}}</b></p>-->
                            <p class="card-text">
                                <span class="badge badge-success">{{$class->day}}</span> From <span class="badge badge-info">{{$class->from}}</span> To <span class="badge badge-info">{{$class->to}}</span>
                            </p>
                        </div>
                    </div>

                </div>
            @endforeach

        </div>
    
    @endif
    
    @if (auth()->user()->user_type == 'student')
    
        <div class="row">

            <div class="col-xl-3 col-md-6">
                <div class="card bg-primary text-white mb-4">
                    <div class="card-body">My Classes</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        {{$studentdashboard['myclasses']}}
                    </div>
                </div>
            </div>

        </div>
    
        <div class="row">
            <div class="col-12 col-md-12 mb-2">
                <h5>Today Classes</h5>
            </div>

            @foreach ($studentdashboard['todayclasses'] as $class)
                <div class="col-4 col-md-4">

                    <div class="card mb-2">
                        <h5 class="card-header">{{$class->name}}</h5>
                        <div class="card-body">
                            <h6 class="card-title">{{$class->subject}} - Grade {{$class->grade}}</h6>
                            <p class="card-text">By <b>{{$class->tutor['first_name']}} {{$class->tutor['last_name']}}</b></p>
                            <p class="card-text">
                                <span class="badge badge-success">{{$class->day}}</span> From <span class="badge badge-info">{{$class->from}}</span> To <span class="badge badge-info">{{$class->to}}</span>
                            </p>
                        </div>
                    </div>

                </div>
            @endforeach

        </div>
    
    @endif
    
<!--    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            DataTable Example
        </div>
        <div class="card-body">
            
            <div class="card">
                <h5 class="card-header">Featured</h5>
                <div class="card-body">
                <h5 class="card-title">Special title treatment</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
            
        </div>
    </div>-->
</div>

@endsection