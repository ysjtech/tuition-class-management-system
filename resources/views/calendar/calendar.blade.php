@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <!--<h1 class="mt-4">Dashboard</h1>-->
    <ol class="breadcrumb mt-4 mb-4">
        <li class="breadcrumb-item active">Calendar</li>
    </ol>
    
    <div class="card mb-4">
<!--        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            DataTable Example
        </div>-->
        <div class="card-body">
            
            <div class="card">
                <div id='calendar'></div>
            </div>
            
        </div>
    </div>

</div>

@endsection

@section('javascript')

<script>
    
    var events = <?php echo json_encode($calendarevents); ?>;
    
    $(function() {

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            weekMode: 'liquid',
            weekends: true,
            events: events
        });

    });
    
</script>

@endsection