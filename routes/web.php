<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('/dashboard');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/calendar', 'CalendarController@index')->name('calendar');

Route::get('/classes', 'ClassesController@index')->name('classes.index');
Route::get('/classes/create', 'ClassesController@create')->name('classes.create');
Route::post('/classes', 'ClassesController@store')->name('classes.store');
Route::get('/classes/{id}', 'ClassesController@edit')->name('classes.edit');
Route::post('/classes/{id}/update', 'ClassesController@update')->name('classes.update');
Route::delete('/classes/{id}', 'ClassesController@destroy')->name('classes.destroy');
Route::post('/classes/delete/{id}', 'ClassesController@delete')->name('classes.delete');
Route::get('/classes/view/{id}', 'ClassesController@show')->name('classes.view');

Route::get('/users', 'UserController@index')->name('users.index');
Route::get('/users/students', 'UserController@students')->name('users.students');
Route::get('/users/tutors', 'UserController@tutors')->name('users.tutors');
Route::get('/users/create', 'UserController@create')->name('users.create');
Route::post('/users', 'UserController@store')->name('users.store');
Route::get('/users/{id}', 'UserController@edit')->name('users.edit');
Route::post('/users/{id}/update', 'UserController@update')->name('users.update');
Route::delete('/users/{id}', 'UserController@destroy')->name('users.destroy');
Route::post('/users/delete/{id}', 'UserController@delete')->name('users.delete');

Route::get('/student/classes/{id}', 'StudentClassesController@edit')->name('student.classes.edit');
Route::post('/student/classes/{id}/update', 'StudentClassesController@update')->name('student.classes.update');

Route::get('/attendance', 'AttendanceController@show')->name('attendance');
Route::post('/attendance', 'AttendanceController@store')->name('attendance.store');