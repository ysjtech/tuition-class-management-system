<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('users')->insert([
            'first_name' => 'SMS',
            'last_name' => 'Administrator',
            'contact_no' => '0712546523',
            'email' => 'admin@sms.com',
            'user_type' => 'admin',
            'password' => Hash::make(123),
        ]);
        
    }
}
